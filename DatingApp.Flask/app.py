from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_cors import CORS


from db import db
from blacklist import BLACKLIST
from resources.user import UserRegister, UserLogin, User




app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["PROPAGATE_EXCEPTIONS"] = True
app.config["JWT_BLACKLIST_ENABLED"] = True
app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = [
    "access",
    "refresh",
]

app.secret_key = "Super Secret Key"
api = Api(app)

@app.before_first_request
def create_tables():
    db.create_all()

jwt = JWTManager(app)

@jwt.user_claims_loader
def add_claims_to_jwt(user):
    if user.id == 1:
        return {"is_admin": True, "name": user.username, "NameIdentifier": user.id}
    return {"is_admin": False, "name": user.username, "NameIdentifier": user.id}

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decypted_token):
    return (decypted_token["jti"] in BLACKLIST)


@jwt.expired_token_loader
def expired_token_callsback():
    return jsonify({"message": "This token has expired.", "error": "token_expired"}), 401

@jwt.unauthorized_loader
def missing_token_callback(error):
    return(
        jsonify(
            {
                "description": "Request does not contain an access token.",
                "error": "authorization required",
            }
        ),
        401,
    )

@jwt.needs_fresh_token_loader
def token_not_fresh_callback():
    return (
        jsonify(
            {"description": "The token is fresh", "error": "fresh_token_required"}
        ),
        401,
    )

@jwt.revoked_token_loader
def revoke_token_callback():
    return(
        jsonify(
            {"description": "The token has been revoked.", "error": "token_revoked"}
        ),
        401,
    )


api.add_resource(UserRegister, "/register")
api.add_resource(UserLogin, '/login')

if __name__ == "__main__":
    db.init_app(app)
    app.run(port=5000, debug=True)

